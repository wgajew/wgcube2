import Config from './config.js';

const Utils = ((faceSize, faceClipSpaceSize, faceTexCoordSize, tileSize, tileClipSpaceSize, tileTexCoordSize, tilesInRow, cubeFaces) => {
  const isPowerOf2 = value => (value & (value - 1)) === 0;

  const radToDeg = r => r * 180 / Math.PI;

  const degToRad = d => d * Math.PI / 180;

  const tileXY = (tileId, mode = 'normal') => {
    const id = tileId - 1;
    if (mode === 'destination') {
      return {
        x: ((id % tilesInRow) * tileClipSpaceSize) - 1,
        y: (faceClipSpaceSize - (tileClipSpaceSize * (Math.floor(id / tilesInRow) + 1))) - 1,
      };
    } if (mode === 'source') {
      return {
        x: (id % tilesInRow) * tileTexCoordSize,
        y: faceTexCoordSize - (tileTexCoordSize * Math.floor(id / tilesInRow)),
      };
    }
    return {
      x: (id % tilesInRow) * tileSize,
      y: faceSize - (tileSize * (Math.floor(id / tilesInRow) + 1)),
    };
  };

  const tileVertices = (tileId, mode = 'destination') => {
    const coordinates = tileXY(tileId, mode);
    if (mode === 'source') {
      return new Float32Array([
        coordinates.x, coordinates.y,
        coordinates.x + tileTexCoordSize, coordinates.y,
        coordinates.x + tileTexCoordSize, coordinates.y - tileTexCoordSize,
        coordinates.x, coordinates.y - tileTexCoordSize,
      ]);
    }
    return new Float32Array([
      coordinates.x, coordinates.y,
      coordinates.x + tileClipSpaceSize, coordinates.y,
      coordinates.x + tileClipSpaceSize, coordinates.y + tileClipSpaceSize,
      coordinates.x, coordinates.y + tileClipSpaceSize,
    ]);
  };

  const getSourceById = (tileId, tilesPerRow, fSize = 1) => {
    const id = tileId - 1;
    const tSize = fSize / tilesPerRow;
    const coordinates = {
      x: (id % tilesPerRow) * tSize,
      y: fSize - (tSize * Math.floor(id / tilesPerRow)),
    };
    return new Float32Array([
      coordinates.x, coordinates.y,
      coordinates.x + tSize, coordinates.y,
      coordinates.x + tSize, coordinates.y - tSize,
      coordinates.x, coordinates.y - tSize,
    ]);
  };

  const getTilesForDiagonal = (tilesPerRow, tilesInDiagonal, face = null) => {
    const allTilesForDiagonal = [];
    const tilesInDiagonalTile = tilesPerRow / tilesInDiagonal;
    let cFaces = { ...cubeFaces };
    if (face in cubeFaces) {
      cFaces = {};
      cFaces[face] = cubeFaces[face];
    }
    for (const f of Object.keys(cFaces)) {
      const faceId = cFaces[f];
      for (let d = 0; d < tilesInDiagonal; d += 1) {
        for (let r = 0; r < tilesInDiagonalTile; r += 1) {
          const rr = r * tilesPerRow;
          for (let c = 1; c <= tilesInDiagonalTile; c += 1) {
            allTilesForDiagonal.push({
              faceId,
              tileId: c + rr + (d * tilesInDiagonalTile * tilesPerRow) + (d * tilesInDiagonalTile),
            });
          }
        }
      }
    }
    return allTilesForDiagonal;
  };

  const getCanvas2d = (image, width, height) => {
    const canvas2d = document.createElement('canvas');
    canvas2d.setAttribute('width', `${width}px`);
    canvas2d.setAttribute('height', `${height}px`);
    const context2d = canvas2d.getContext('2d');
    context2d.clearRect(0, 0, width, height);
    context2d.drawImage(image, 0, 0, width, height);
    return canvas2d;
  };

  return Object.freeze({
    isPowerOf2,
    radToDeg,
    degToRad,
    tileXY,
    tileVertices,
    getSourceById,
    getTilesForDiagonal,
    getCanvas2d,
  });
})(Config.faceSize, Config.faceClipSpaceSize, Config.faceTexCoordSize, Config.tileSize, Config.tileClipSpaceSize, Config.tileTexCoordSize, Config.tilesInRow, Config.cubeFaces);

export default Utils;
