import Config from './config.js';
import WebGL from './webgl.js';
import Utils from './utils.js';
import Buffers from './buffers.js';
import tileComposer from './tilecomposer.js';
import Geometry from './geometry.js';

const Textures = ((gl, tilesInRow, tileSize, faceSize, cubeFaces, tilesInFace, viewportChange) => {
  const setParameteri2D = (
    wrapS = gl.REPEAT,
    wrapT = gl.REPEAT,
    minFilter = gl.NEAREST,
    magFilter = gl.NEAREST,
  ) => {
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrapS);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrapT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, minFilter);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, magFilter);
  };

  const setParameteri3D = (
    wrapS = gl.REPEAT,
    wrapT = gl.REPEAT,
    minFilter = gl.NEAREST,
    magFilter = gl.NEAREST,
  ) => {
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, wrapS);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, wrapT);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, minFilter);
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, magFilter);
  };

  const getTexture2D = () => {
    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    return texture;
  };

  const createAndSetupTexture2D = (image = null, parameteri = true, size = tileSize) => {
    const texture = getTexture2D();
    if (image) {
      // Upload the image into the texture.
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    } else {
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, size, size, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    }
    if (parameteri) setParameteri2D();
    return texture;
  };

  const getTexture3D = () => {
    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
    return texture;
  };

  const createAndNullifyTexture3D = (parameteri = true) => {
    const texture = getTexture3D();
    for (let i = 0; i < 6; i++) {
      gl.texImage2D(gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, gl.RGBA, faceSize, faceSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
    }
    if (parameteri) setParameteri3D();
    return texture;
  };

  const loadedFaces = [];
  let allTiles = [];
  const prepareFaceTextures = (faceId) => {
    const textures = [];
    for (let i = 0; i < tilesInFace; i += 1) {
      const tileId = i + 1;
      const texture = createAndSetupTexture2D();
      if (viewportChange) {
        const coords = Utils.tileXY(tileId);
        textures.push({
          faceId, tileId, x: coords.x, y: coords.y, texture,
        });
      } else {
        const destination = Utils.tileVertices(tileId);
        textures.push({
          faceId, tileId, destination, texture,
        });
      }
    }
    if (loadedFaces.indexOf(faceId) === -1) {
      allTiles = [...allTiles, ...textures];
    }
    loadedFaces.push(faceId);
  };
  const prepareAllTiles = () => {
    for (const face of Object.keys(cubeFaces)) {
      const faceId = cubeFaces[face];
      prepareFaceTextures(faceId);
    }
  };

  const getallTiles = () => allTiles;

  const getFilteredTiles = (excludedTiles, intersection = false) => {
    const exclusion = JSON.stringify(excludedTiles);
    const filteredTiles = getallTiles().filter((obj) => {
      const { faceId, tileId } = obj;
      if (intersection) {
        return exclusion.indexOf(JSON.stringify({ faceId, tileId })) !== -1;
      }
      return exclusion.indexOf(JSON.stringify({ faceId, tileId })) === -1;
    });
    return filteredTiles;
  };

  const getIdFromMainArray = (faceId, tileId) => allTiles.findIndex(tile => tile.faceId === faceId && tile.tileId === tileId);

  const getTile = (faceId, tileId) => allTiles[getIdFromMainArray(faceId, tileId)];

  const getTileWithMultipleTextures = (faceId, tileId, textures) => {
    if (viewportChange) {
      const coords = Utils.tileXY(tileId);
      return {
        faceId, tileId, x: coords.x, y: coords.y, textures,
      };
    }
    const destination = Utils.tileVertices(tileId);
    return {
      faceId, tileId, destination, textures,
    };
  };

  const getBlended = (program, programData, toBlend, tileId) => {
    const finalTexture = createAndSetupTexture2D(null);
    const fBuffer = Buffers.getFrameBuffer();
    Buffers.setFramebufferTexture2D(finalTexture);
    gl.bindFramebuffer(gl.FRAMEBUFFER, fBuffer);
    const src = Utils.tileVertices(tileId, 'source');
    if (Array.isArray(toBlend)) {
      for (const { texture, add, source } of toBlend) {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        tileComposer.drawAndBlend(program, programData, add, source || src);
      }
    } else {
      gl.bindTexture(gl.TEXTURE_2D, toBlend.texture);
      tileComposer.drawAndBlend(program, programData, toBlend.add, toBlend.source || src);
    }
    return finalTexture;
  };

  return Object.freeze({
    getTexture2D,
    createAndSetupTexture2D,
    getTexture3D,
    createAndNullifyTexture3D,
    setParameteri2D,
    setParameteri3D,
    prepareAllTiles,
    getallTiles,
    getFilteredTiles,
    getIdFromMainArray,
    getTile,
    getTileWithMultipleTextures,
    getBlended,
  });
})(WebGL.getGL(), Config.tilesInRow, Config.tileSize, Config.faceSize, Config.cubeFaces, Config.tilesInFace, Config.viewportChange);

export default Textures;
