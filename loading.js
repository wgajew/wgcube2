import Config from './config.js';

const Load = (() => {
  const faceImages = [];
  const otherImages = [];

  const loadAllImages = (callback) => {
    const frontImg = new Image();
    frontImg.src = 'lake/front.jpg';
    frontImg.onload = () => {
      faceImages.push(frontImg);
      const backImg = new Image();
      backImg.src = 'lake/back.jpg';
      backImg.onload = () => {
        faceImages.push(backImg);
        const topImg = new Image();
        topImg.src = 'lake/top.jpg';
        topImg.onload = () => {
          faceImages.push(topImg);
          const bottomImg = new Image();
          bottomImg.src = 'lake/bottom.jpg';
          bottomImg.onload = () => {
            faceImages.push(bottomImg);
            const leftImg = new Image();
            leftImg.src = 'lake/left.jpg';
            leftImg.onload = () => {
              faceImages.push(leftImg);
              const rightImg = new Image();
              rightImg.src = 'lake/right.jpg';
              rightImg.onload = () => {
                faceImages.push(rightImg);
                const im1 = new Image();
                im1.src = 'new/im1.png';
                im1.onload = () => {
                  otherImages.push(im1);
                  const im2 = new Image();
                  im2.src = 'new/im2.png';
                  im2.onload = () => {
                    otherImages.push(im2);
                    const im3 = new Image();
                    im3.src = 'new/im3.png';
                    im3.onload = () => {
                      otherImages.push(im3);
                      const im4 = new Image();
                      im4.src = 'new/im4.png';
                      im4.onload = () => {
                        otherImages.push(im4);
                        const im5 = new Image();
                        im5.src = 'new/im5.png';
                        im5.onload = () => {
                          otherImages.push(im5);
                          const im6 = new Image();
                          im6.src = 'new/im6.png';
                          im6.onload = () => {
                            otherImages.push(im6);
                            const im7 = new Image();
                            im7.src = 'new/im7.png';
                            im7.onload = () => {
                              otherImages.push(im7);
                              const im8 = new Image();
                              im8.src = 'new/im8.png';
                              im8.onload = () => {
                                otherImages.push(im8);
                                const imAll = new Image();
                                imAll.src = 'new/imAll2.png';
                                imAll.onload = () => {
                                  otherImages.push(imAll);
                                  callback();
                                };
                              };
                            };
                          };
                        };
                      };
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };

  return Object.freeze({
    faceImages,
    otherImages,
    loadAllImages,
  });
})(Config.cubeFaces, Config.tilesInFace, Config.imgPath);

export default Load;
