import Config from './config.js';
import Load from './loading.js';
import WebGL from './webgl.js';
import Shaders from './shaders.js';
import Programs from './programs.js';
import Geometry from './geometry.js';
import Textures from './textures.js';
import Buffers from './buffers.js';
import tileComposer from './tilecomposer.js';
import Utils from './utils.js';
import Events from './events.js';

function main(gl, canv, canvasSize, tileSize, cubeFaces, tilesInRow, tilesInDiagonal) {
  const canvas = canv;
  canvas.width = canvasSize;
  canvas.height = canvasSize;
  const fieldOfViewRadians = Utils.degToRad(60);

  console.time('main');

  Events.addAllListeners();

  // shader programs
  const program1 = Programs.getProgram(Shaders.getVertexShader(), Shaders.getFragmentShader());
  const program2 = Programs.getProgram(Shaders.getVertexShader2(), Shaders.getFragmentShader2());
  const program3 = Programs.getProgram(Shaders.getVertexShader(), Shaders.getFragmentShader3());

  // attributes
  const vertexLocation = gl.getAttribLocation(program1, 'a_position');
  const texcoordLocation = gl.getAttribLocation(program1, 'a_texCoord');
  const vertexLocation2 = gl.getAttribLocation(program2, 'a_position');

  // uniforms
  const textureLocation = gl.getUniformLocation(program1, 'u_image');
  const flipYLocation = gl.getUniformLocation(program1, 'u_flipY');
  const texture1Location = gl.getUniformLocation(program3, 'texture1');
  const texture2Location = gl.getUniformLocation(program3, 'texture2');
  const texture3Location = gl.getUniformLocation(program3, 'texture3');
  const texture4Location = gl.getUniformLocation(program3, 'texture4');
  const texture5Location = gl.getUniformLocation(program3, 'texture5');
  const texture6Location = gl.getUniformLocation(program3, 'texture6');
  const texture7Location = gl.getUniformLocation(program3, 'texture7');
  const texture8Location = gl.getUniformLocation(program3, 'texture8');
  const projectionMatrixLocation = gl.getUniformLocation(program2, 'u_Pmatrix');
  const viewMatrixLocation = gl.getUniformLocation(program2, 'u_Vmatrix');
  const cubeTextureLocation = gl.getUniformLocation(program2, 'u_cube_texture');

  // buffers
  const vertexBuffer = Buffers.getBuffer(Geometry.vertices);
  const indexBuffer = Buffers.getBuffer(Geometry.indices, gl.ELEMENT_ARRAY_BUFFER);
  const texcoordBuffer = Buffers.getBuffer(Geometry.coordinates);
  const vertexBuffer3d = Buffers.getBuffer(Geometry.vertices3d);
  const indexBuffer3d = Buffers.getBuffer(Geometry.indices3d, gl.ELEMENT_ARRAY_BUFFER);

  // Create textures.
  Textures.prepareAllTiles();

  const cubeTexture = Textures.createAndNullifyTexture3D();

  // Create and bind the framebuffer
  const frameBuffer = Buffers.getFrameBuffer();

  const programData = {
    vertexBuffer,
    indexBuffer,
    texcoordBuffer,
    vertexLocation,
    texcoordLocation,
    textureLocation,
    flipYLocation,
    frameBuffer,
    cubeTexture,
    texture1Location,
    texture2Location,
    texture3Location,
    texture4Location,
    texture5Location,
    texture6Location,
    texture7Location,
    texture8Location,
  };

  const blankTiles = Utils.getTilesForDiagonal(tilesInRow, tilesInDiagonal);

  const faceTextures = {
    front: Textures.createAndSetupTexture2D(Load.faceImages[0]),
    back: Textures.createAndSetupTexture2D(Load.faceImages[1]),
    top: Textures.createAndSetupTexture2D(Load.faceImages[2]),
    bottom: Textures.createAndSetupTexture2D(Load.faceImages[3]),
    left: Textures.createAndSetupTexture2D(Load.faceImages[4]),
    right: Textures.createAndSetupTexture2D(Load.faceImages[5]),
  };

  const eightTexturesForDiagonal = {
    one: Textures.createAndSetupTexture2D(Load.otherImages[0]),
    two: Textures.createAndSetupTexture2D(Load.otherImages[1]),
    three: Textures.createAndSetupTexture2D(Load.otherImages[2]),
    four: Textures.createAndSetupTexture2D(Load.otherImages[3]),
    five: Textures.createAndSetupTexture2D(Load.otherImages[4]),
    six: Textures.createAndSetupTexture2D(Load.otherImages[5]),
    seven: Textures.createAndSetupTexture2D(Load.otherImages[6]),
    eight: Textures.createAndSetupTexture2D(Load.otherImages[7]),
  };
  const oneTextureForDiagonal = Textures.createAndSetupTexture2D(Load.otherImages[8]);

  function drawOnTiles() {
    const filteredTiles = Textures.getFilteredTiles(blankTiles);
    // const filteredTiles = Textures.getallTiles();
    for (let i = 0, l = filteredTiles.length; i < l; i++) {
      const face = Object.keys(cubeFaces).find(key => cubeFaces[key] === filteredTiles[i].faceId);
      const texturesToBlend = [{ texture: faceTextures[face], add: true }];
      filteredTiles[i].texture = Textures.getBlended(program1, programData, texturesToBlend, filteredTiles[i].tileId);
      tileComposer.drawOnTile(filteredTiles[i], program1, programData);
    }
  }

  function drawMultipleTexturesOnTiles(clear = false) {
    const validBlankTiles = Textures.getFilteredTiles(blankTiles, true);
    const glBlend = false;
    if (clear) {
      for (const { faceId, tileId } of validBlankTiles) {
        const tile = Textures.getTileWithMultipleTextures(faceId, tileId, []);
        tileComposer.drawMultipleTexturesOnTile(tile, program3, programData, clear);
      }
    } else {
      let sourceTile = 0;
      for (const { faceId, tileId } of validBlankTiles) {
        if (glBlend) {
          const toBlend = [
            { texture: eightTexturesForDiagonal.one, add: true },
            { texture: eightTexturesForDiagonal.two, add: true },
            { texture: eightTexturesForDiagonal.three, add: true },
            { texture: eightTexturesForDiagonal.four, add: true },
            { texture: eightTexturesForDiagonal.five, add: true },
            { texture: eightTexturesForDiagonal.six, add: true },
            { texture: eightTexturesForDiagonal.seven, add: true },
            { texture: eightTexturesForDiagonal.eight, add: true },
          ];
          const texture = Textures.getBlended(program1, programData, toBlend, tileId);
          const tile = Textures.getTile(faceId, tileId);
          tile.texture = texture;
          tileComposer.drawOnTile(tile, program1, programData);
        } else {
          sourceTile = sourceTile < tilesInRow ? sourceTile + 1 : 1;
          const inRow = tilesInRow === tilesInDiagonal ? tilesInRow / 2 : tilesInRow;
          const toBlend = { texture: oneTextureForDiagonal, add: true, source: Utils.getSourceById(sourceTile, inRow) };
          const texture = Textures.getBlended(program1, programData, toBlend, tileId);
          const tile = Textures.getTile(faceId, tileId);
          tile.texture = texture;
          tileComposer.drawOnTile(tile, program1, programData);
        }
      }
    }
  }

  drawOnTiles();
  drawMultipleTexturesOnTiles();
  console.log(`tile size: ${tileSize}`);

  // Draw the scene.
  function drawScene(mode = 1) {
    if (Events.getDrag() || mode === 0) {
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      // Tell it to use our program (pair of shaders)
      gl.useProgram(program2);

      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

      // Bind the position buffer.
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer3d);

      // Tell the position attribute how to get data out of vertexBuffer (ARRAY_BUFFER)
      const size = 3; // 3 components per iteration
      Buffers.setVertexAttribPointer(vertexLocation2, size);
      gl.enableVertexAttribArray(vertexLocation2);

      // Compute the projection matrix
      const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
      const { mat4 } = glMatrix;
      const projectionMatrix = mat4.perspective(mat4.create(), fieldOfViewRadians, aspect, 1, 2000);

      const cameraPosition = [0, 0, 1];
      const up = [0, 1, 0];
      const target = [0, 0, 0];

      // Compute the camera's matrix using look at.
      const viewMatrix = mat4.lookAt(mat4.create(), cameraPosition, target, up);

      mat4.rotateX(viewMatrix, viewMatrix, Events.getPhi());
      mat4.rotateY(viewMatrix, viewMatrix, Events.getTheta());

      // Set the matrix.
      gl.uniformMatrix4fv(projectionMatrixLocation, false, projectionMatrix);
      gl.uniformMatrix4fv(viewMatrixLocation, false, viewMatrix);

      // Tell the shader to use texture unit 0 for u_cube_texture
      gl.uniform1i(cubeTextureLocation, 0);
      gl.activeTexture(gl.TEXTURE0);

      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer3d);
      gl.drawElements(gl.TRIANGLES, Geometry.indices3d.length, gl.UNSIGNED_SHORT, 0);
    }
    requestAnimationFrame(drawScene);
  }

  drawScene(0);
  console.timeEnd('main');

  const chbx = document.getElementById('check');
  chbx.onchange = function () {
    if (this.checked) {
      drawMultipleTexturesOnTiles();
    } else {
      drawMultipleTexturesOnTiles(true);
    }
    drawScene(0);
  };
}

function init() {
  Load.loadAllImages(() => {
    main(WebGL.getGL(), WebGL.getCanvas(), Config.canvasSize, Config.tileSize, Config.cubeFaces, Config.tilesInRow, Config.tilesInDiagonal);
  });
}
init();
