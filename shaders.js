import Config from './config.js';

const Shaders = ((Params) => {
  let Parameters = Params;

  const getVertexShader = () => `
    attribute vec2 a_position;
    attribute vec2 a_texCoord;
    varying vec2 v_texCoord;
    
    void main() {
      gl_Position = vec4(a_position, 0, 1);
      v_texCoord = a_texCoord;
    }
  `;

  const getFragmentShader = () => `
    precision mediump float;
    // our textures
    uniform sampler2D u_image;
    // the texCoords passed in from the vertex shader.
    varying vec2 v_texCoord;
    
    uniform float u_flipY;
    
    void main() {
        vec4 color = texture2D(u_image, vec2(v_texCoord.x, v_texCoord.y * u_flipY));
        if ((v_texCoord.x < 0.05 || v_texCoord.y < 0.05) && u_flipY == 1.0) {
          gl_FragColor = color + vec4(1.0, 1.0, 1.0, 1.0) * 0.05;
        } else {
          gl_FragColor = color; 
        }
    }
  `;

  const getVertexShader2 = () => `
    attribute vec4 a_position;
    uniform mat4 u_Pmatrix;
    uniform mat4 u_Vmatrix;
    varying vec3 v_normal;
    
    void main() {
      // Multiply the position by the matrix.
      gl_Position = u_Pmatrix * u_Vmatrix * a_position;
      // Pass a normal. Since the positions
      // centered around the origin we can just
      // pass the position
      v_normal = normalize(a_position.xyz);
    }
  `;

  const getFragmentShader2 = () => `
    precision mediump float;
    // Passed in from the vertex shader.
    varying vec3 v_normal;
    // The texture.
    uniform samplerCube u_cube_texture;
    
    void main() {
      vec4 landscape = textureCube(u_cube_texture, normalize(vec3(-v_normal.x,-v_normal.y,v_normal.z)));
      gl_FragColor = landscape;
    }
  `;

  const getFragmentShader3 = () => `
    precision mediump float;
    // our textures
    uniform sampler2D texture1;
    uniform sampler2D texture2;
    uniform sampler2D texture3;
    uniform sampler2D texture4;
    uniform sampler2D texture5;
    uniform sampler2D texture6;
    uniform sampler2D texture7;
    uniform sampler2D texture8;
    // the texCoords passed in from the vertex shader.
    varying vec2 v_texCoord;
    
    void main() {
      vec4 image1 = texture2D(texture1, v_texCoord);
      vec4 image2 = texture2D(texture2, v_texCoord);
      vec4 image3 = texture2D(texture3, v_texCoord);
      vec4 image4 = texture2D(texture4, v_texCoord);
      vec4 image5 = texture2D(texture5, v_texCoord);
      vec4 image6 = texture2D(texture6, v_texCoord);
      vec4 image7 = texture2D(texture7, v_texCoord);
      vec4 image8 = texture2D(texture8, v_texCoord);
      gl_FragColor = image1 + image2 + image3 + image4 + image5 + image6 + image7 + image8;
    }
  `;

  const getParameters = () => Parameters;
  const updateParameters = (KeyValue) => {
    Parameters = { ...Parameters, ...KeyValue };
  };

  return Object.freeze({
    getVertexShader,
    getFragmentShader,
    getVertexShader2,
    getFragmentShader2,
    getFragmentShader3,
    getParameters,
    updateParameters,
  });
})(Config.shaderParameters);

export default Shaders;
